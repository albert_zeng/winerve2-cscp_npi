import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_signin'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_Login/input_email'), GlobalVariable.login_email)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Login/input_password'), GlobalVariable.login_password)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Page_MainPage/text_id'), user_id)

WebUI.verifyElementText(findTestObject('Page_MainPage/text_name'), user_name)

WebUI.delay(1)

WebUI.click(findTestObject('Page_MainPage/btn_logout'))

WebUI.delay(2)

WebUI.verifyMatch(WebUI.getUrl(), 'https://login.microsoftonline.com/wistron.com/oauth2/logout', false)

