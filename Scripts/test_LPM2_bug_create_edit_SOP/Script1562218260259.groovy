import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.interactions.Actions as Actions

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_signin'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_Login/input_email'), GlobalVariable.login_email)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Login/input_password'), GlobalVariable.login_password)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(15)

WebUI.click(findTestObject('Page_MyProject/btn_project_LPM-2_4PD0GU010001'))

WebUI.delay(10)

WebUI.click(findTestObject('Page_MyProject/btn_stage_SVT_1'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Page_MyProject/text_title_bug_list'), 10)

WebUI.delay(15)

failed_list_1_xpath = findTestObject('Page_MyProject/list_failed_list_1').findPropertyValue('xpath')

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

driver.findElement(By.xpath(failed_list_1_xpath + '//div[6]/div[1]')).click()

WebUI.click(findTestObject('Page_MyProject/btn_failed_list_edit'))

WebUI.delay(10)

WebUI.scrollToElement(findTestObject('Page_MyProject/title_classification'), 10)

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/text_SOP_step_1'), 'The category of issue')

WebUI.click(findTestObject('Page_MyProject/SOP/select_SOP_step_1'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/select_SOP_step_1'), 'EE')

WebUI.click(findTestObject('Page_MyProject/SOP/btn_SOP_step_1'))

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/text_SOP_EE'), 'Is it the common issue as follows?')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/select_SOP_EE'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/select_SOP_EE'), '(1)Abnormal shutdown')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/btn_SOP_EE'))

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1'), 'Please check these below and key in the result.')

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1_item1'), 'Check thermal Temp. if scald hand?')

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1_item2'), 'Check thermal grease if enough?')

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1_item3'), 'CPU/GPU Re-Assembly to tight if can fix?')

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1_item4'), 'Power adapter supply how many watt?')

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/text_SOP_EE_select1_item5'), 'Has it reboot after shutdown?')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item1'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item1'), 'Yes')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item1'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item1'), 'No')

WebUI.verifyElementNotClickable(findTestObject('Page_MyProject/SOP/EE/Select1/btn_SOP_EE_select1'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item2'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item2'), 'Yes')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item2'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item2'), 'No')

WebUI.verifyElementNotClickable(findTestObject('Page_MyProject/SOP/EE/Select1/btn_SOP_EE_select1'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item3'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item3'), 'Yes')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item3'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item3'), 'No')

WebUI.verifyElementNotClickable(findTestObject('Page_MyProject/SOP/EE/Select1/btn_SOP_EE_select1'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_MyProject/SOP/EE/Select1/input_SOP_EE_select1_item4'), 'test')

WebUI.verifyElementNotClickable(findTestObject('Page_MyProject/SOP/EE/Select1/btn_SOP_EE_select1'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item5'))

action.sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item5'), 'Yes')

WebUI.click(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item5'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.verifyElementText(findTestObject('Page_MyProject/SOP/EE/Select1/select_SOP_EE_select1_item5'), 'No')

WebUI.verifyElementClickable(findTestObject('Page_MyProject/SOP/EE/Select1/btn_SOP_EE_select1'))

WebUI.delay(10)

