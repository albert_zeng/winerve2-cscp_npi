import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.TestObjectXpath as TestObjectXpath
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import java.text.SimpleDateFormat as SimpleDateFormat
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.interactions.Actions as Actions

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_signin'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_Login/input_email'), GlobalVariable.login_email)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Login/input_password'), GlobalVariable.login_password)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_Login/btn_next'))

WebUI.delay(15)

WebUI.click(findTestObject('Page_MyProject/btn_project_LPM-2_4PD0GU010001'))

WebUI.delay(10)

WebUI.click(findTestObject('Page_MyProject/btn_stage_SVT_1'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Page_MyProject/text_title_bug_list'), 10)

WebUI.delay(15)

failed_list_1_xpath = findTestObject('Page_MyProject/list_failed_list_1').findPropertyValue('xpath')

WebDriver driver = DriverFactory.getWebDriver()

station = driver.findElement(By.xpath(failed_list_1_xpath + '//div[3]')).getText()

error_code = driver.findElement(By.xpath(failed_list_1_xpath + '//div[4]')).getText()

description = driver.findElement(By.xpath(failed_list_1_xpath + '//div[5]')).getText()

sn1 = (driver.findElement(By.xpath(failed_list_1_xpath + '//div[6]/div[1]')).getText() + ';')

driver.findElement(By.xpath(failed_list_1_xpath + '//div[6]/div[1]')).click()

WebUI.click(findTestObject('Page_MyProject/btn_failed_list_edit'))

WebUI.delay(10)

Date date = new Date()

SimpleDateFormat dateFormat = new SimpleDateFormat('yyyy/MM/dd')

WebUI.verifyElementText(findTestObject('Page_MyProject/text_build_date'), dateFormat.format(date))

WebUI.verifyElementText(findTestObject('Page_MyProject/text_station'), station)

WebUI.verifyElementText(findTestObject('Page_MyProject/text_error_code'), error_code)

WebUI.setText(findTestObject('Page_MyProject/input_defect_rate_numerator'), defect_rate_numerator)

WebUI.setText(findTestObject('Page_MyProject/input_defect_rate_denominator'), defect_rate_denominator)

WebUI.click(findTestObject('Page_MyProject/select_severity'))

WebUI.delay(1)

Actions action = new Actions(driver)

if (severity == 'A') {
    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
} else if (severity == 'B') {
    action.sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
} else if (severity == 'C') {
    action.sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
}

WebUI.click(findTestObject('Page_MyProject/select_due_date'))

WebUI.setText(findTestObject('Page_MyProject/select_due_date_input'), due_date)

WebUI.click(findTestObject('Page_MyProject/title_bug_information'))

if (WebUI.getText(findTestObject('Page_MyProject/tag_symptom')).equals('New Tag')) {
    WebUI.click(findTestObject('Page_MyProject/tag_symptom'))

    WebUI.sendKeys(findTestObject('Page_MyProject/tag_symptom_input'), symptom)

    action.sendKeys(Keys.ENTER).perform()
}

WebUI.setText(findTestObject('Page_MyProject/input_duplication_step1'), duplication_step1)

WebUI.scrollToElement(findTestObject('Page_MyProject/title_classification'), 10)

WebUI.click(findTestObject('Page_MyProject/SOP/select_SOP_step_1'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.click(findTestObject('Page_MyProject/SOP/btn_SOP_step_1'))

WebUI.setText(findTestObject('Page_MyProject/SOP/input_SOP_others_input'), 'test')

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('Page_MyProject/text_SN_1'), sn1)

WebUI.setText(findTestObject('Page_MyProject/input_message'), message)

WebUI.click(findTestObject('Page_MyProject/btn_save'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Page_MyProject/table_stage_station'), 10)

WebUI.delay(5)

WebUI.click(findTestObject('Page_MyProject/Bug_List/btn_show_all'))

WebUI.waitForElementVisible(findTestObject('Page_MyProject/Bug_List/table_bug_list'), 60)

List<WebElement> bugListTable = driver.findElements(By.xpath('//*[@id="app"]/div/div[2]/div[2]/div/div[5]/div/div/div/div/div/div/div[2]/table/tbody/tr'))

TestObject bugListTable_last = new TestObject('bugListTable_last')

bugListTable_last.addProperty('xpath', ConditionType.EQUALS, ('//*[@id="app"]/div/div[2]/div[2]/div/div[5]/div/div/div/div/div/div/div[2]/table/tbody/tr[' + 
    bugListTable.size()) + ']/td[2]', true)

WebUI.scrollToElement(bugListTable_last, 10)

WebUI.delay(5)

WebUI.click(bugListTable_last)

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('Page_MyProject/text_build_date'), dateFormat.format(date))

WebUI.verifyElementText(findTestObject('Page_MyProject/text_station'), station)

WebUI.verifyElementText(findTestObject('Page_MyProject/text_error_code'), error_code)

WebUI.verifyMatch(WebUI.getAttribute(findTestObject('Page_MyProject/input_defect_rate_numerator'), 'value'), defect_rate_numerator, 
    false)

WebUI.verifyMatch(WebUI.getAttribute(findTestObject('Page_MyProject/input_defect_rate_denominator'), 'value'), defect_rate_denominator, 
    false)

WebUI.verifyMatch(WebUI.getAttribute(findTestObject('Page_MyProject/select_due_date'), 'value'), due_date, false)

WebUI.scrollToElement(findTestObject('Page_MyProject/title_classification'), 10)

WebUI.verifyElementText(findTestObject('Page_MyProject/text_notification'), 'You can hover single SN to trace the FAE code description and Reason code description.')

WebUI.click(findTestObject('Page_MyProject/btn_notification'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_MyProject/select_category'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

WebUI.click(findTestObject('Page_MyProject/btn_save'))

WebUI.delay(5)

