<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_search</name>
   <tag></tag>
   <elementGuidId>2f374fa0-5d4e-4743-ad1b-90561e5ac1d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[3]/div/div/div/div[2]/div/div/div/div[7]/button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div/div/div/div[2]/div/div/div/div[8]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[3]/div/div/div/div[2]/div/div/div/div[7]/button</value>
   </webElementProperties>
</WebElementEntity>
